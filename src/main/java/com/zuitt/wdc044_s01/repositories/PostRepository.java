package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository


public interface PostRepository extends CrudRepository<Post, Object> {
}


// In express, data goes through the process of route -> controller -> model -> database
// In Spring boot, data goes through the process of controller -> Service -> repository -> model -> database