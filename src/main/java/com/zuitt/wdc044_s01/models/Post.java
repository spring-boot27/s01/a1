package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

//mark this Java obj as a database table. This is also the first step in dealing with or creatnig models just like in js
@Entity
@Table(name="posts")


public class Post {
    //indicate the primary key
    @Id
    //auto-increment the ID column
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    //default constructor need when retrieving posts
    public Post(){}

    //other necesasry constructors
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public String getContent(){
        return content;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
